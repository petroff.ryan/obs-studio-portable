# obs-studio-portable

This is my OBS studio portable setup for linux, with linux plugins like websockets (https://github.com/Palakis/obs-websocket) 


After cloning this repository:
cd ./obs-studio-portable/bin/64bit/ 
./obs

Note: this can be run in 'portable mode' with ./obs -p

Note: Logs and profiler data are .gitignore'd 

---

Note: trying to use it on another computer has revealed it's not actually portable. A mystery for another day.